### Proxima ###

## Synopsis

This is the repository for the Proxima App. This app is a calendar app for Lyon I universities (at the moment). There will be many features : simple ones such as basic calendar display, courses details, but also more complex ones like notifications, LDAP sign-in, a "I'm late" feature, and calendar management for Professors.

## Motivation

This project is the result of the "BDD Mobile" course, where more than 20 students (us) were given the opportunity to choose a project, and manage it however we wanted to. We chose to develop this app because there is no good one actually developped that suits our needs.

## Installation

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim augue ut purus scelerisque, nec hendrerit nulla tincidunt. Vivamus rhoncus lorem mattis, interdum ipsum vel.

## API Reference

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim augue ut purus scelerisque, nec hendrerit nulla tincidunt. Vivamus rhoncus lorem mattis, interdum ipsum vel.

## Tests

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim augue ut purus scelerisque, nec hendrerit nulla tincidunt. Vivamus rhoncus lorem mattis, interdum ipsum vel.

## Contributors

List of people working on the project : 

## License

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim augue ut purus scelerisque, nec hendrerit nulla tincidunt. Vivamus rhoncus lorem mattis, interdum ipsum vel.